close all
clc

% Zmienna, ktorej wartosc odpowiada za ilosc wykonania tej samej symulacji
% (tzn. np. zgiecia nogi w kolanie stojac). Przykladowo dla n = 5
% symulacja wykona sie pieciokrotnie.
n = 3;

% hold on;
for i = 1:n
    
    figure(1);
    axis([-1 1 -1 1 0 2]);
    hold on;
    % Ruch nogi
    Noga.plot(q, 'floorlevel', 0, 'noname', 'noshadow', 'view', [128 26]);
        
    figure(2);
    set(figure(2), 'Units', 'pixels', 'Position', [10 205 640 480]);
    % Przebiegi zmiennych konfiguracyjnych w zaleznosci od czasu
    subplot(2, 2, 1);
    plot(t, q);
    grid on;
    title('Wykres q(t)');
    xlabel('t [s]');
    ylabel('q [rad]');
    legend('q1', 'q2', 'q3', 'q4', 'q5', 'q6');
    set(legend, 'FontSize', 8);

    % Sciezka koncowki roboczej (stopy) w plaszczyznie YZ
    subplot(2, 2, 2);
    plot(p(:, 2), p(:, 3));
    grid on;
    title('Sciezka koncowki roboczej (stopy) w plaszczyznie YZ');
    xlabel('y [m]');
    ylabel('z [m]');

    % Przebieg pozycji koncowki roboczej (stopy) we wspolrzednych
    % kartezjanskich w zaleznosci od czasu
    subplot(2, 2, 3);
    plot(t, p);
    grid on;
    title('Wykres p(t)');
    xlabel('t [s]');
    ylabel('p [m]');
    legend('x', 'y', 'z');

    figure(3);
    set(figure(3), 'Units', 'pixels', 'Position', [665 205 640 480]);
    % Przebiegi predkosci w zaleznosci od czasu
    subplot(2, 2, 1);
    plot(t, qd);
    grid on;
    title('Wykres qd(t)');
    xlabel('t [s]');
    ylabel('qd [rad/s]');
    legend('q1d', 'q2d', 'q3d', 'q4d', 'q5d', 'q6d');
    set(legend, 'FontSize', 8)

    % Przebiegi przyspieszen w zaleznosci od czasu
    subplot(2, 2, 2);
    plot(t, qdd);
    grid on;
    title('Wykres qdd(t)');
    xlabel('t [s]');
    ylabel('qdd [rad/s^2]');
    legend('q1dd', 'q2dd', 'q3dd', 'q4dd', 'q5dd', 'q6dd');
    set(legend, 'FontSize', 8)
    
    % Wykres predkosci liniowych w zlaczach w zaleznosci od czasu
    if wybor == 1 || wybor == 3
        v = qd*0.4190;    % 0.41 + 0.04 + 0.05 = 0.4190
    elseif wybor == 2
        v = qd*0.8490;    % 0.43 + 0.4190 = 0.8490
    end
    subplot(2, 2, 3);
    plot(t, v);
    grid on;
    title('Wykres v(t)');
    xlabel('t [s]');
    ylabel('v [m/s]');
    legend('v1', 'v2', 'v3', 'v4', 'v5', 'v6');
    set(legend, 'FontSize', 8);
    
    % Wykres przyspieszen liniowych w zlaczach zaleznosci od czasu
    if wybor == 1 || wybor == 3
        a = qdd*0.4190;    
    elseif wybor == 2
        a = qdd*0.8490;
    end
    subplot(2, 2, 4);
    plot(t, a);
    grid on;
    title('Wykres a(t)');
    xlabel('t [s]');
    ylabel('a [m/s^2]');
    legend('a1', 'a2', 'a3', 'a4', 'a5', 'a6');
    set(legend, 'FontSize', 8);
    
end
% hold off;