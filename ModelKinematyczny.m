close all
clear
clc

% Zmienna sluzaca do wyboru symulacji ruchu nogi (1 - zgiecie nogi
% w kolanie stojac, 2 - uniesienie wyprostowanej nogi stojac,
% 3 - wyprostowanie nogi w kolanie siedzac)
wybor = 1;

% Zlacza (stawy) nogi (L(1)-L(3) - staw biodrowy, L(4) - staw kolanowy,
% L(5)-L(6) - staw skokowy)
L(1) = Link([0 0 0     pi/2], 'standard');
L(2) = Link([0 0 0     3*pi/2], 'standard');
L(3) = Link([0 0 0.43  0], 'standard');
L(4) = Link([0 0 0.41  0], 'standard');
L(5) = Link([0 0 0.004 pi/2], 'standard');
L(6) = Link([0 0 0.005 0], 'standard');

% Polaczenie struktury kinematycznej
Noga = SerialLink(L, 'name', 'Noga', 'base', transl(0, 0, 1)*trotz(pi));

% Zmienne konfiguracyjne przy pozycji wyjsciowej dla dwoch pierwszych
% przypadkow
q1 = [0 -pi/2 0 0 0 0];    % q1(2) = -pi/2, gdyz dla q1(2) = 0 mechanizm
% wyglada jak ramie, a nie jak noga
% Zmienne konfiguracyjne przy nodze zgietej w kolanie stojac
q2 = [0 -pi/2 0 pi/2 0 0];
% Zmienne konfiguracyjne przy nodze wyprostowanej i uniesionej
q3 = [0 -pi/2 -pi/2 0 0 0];
% Zmienne konfiguracyjne przy nodze zgietej w kolanie siedzac (pozycja
% wyjsciowa dla trzeciej symulacji)
q4 = [0 -pi/2 -pi/2 pi/2 0 0];
% Zmienne konfiguracyjne przy nodze wyprostowanej i uniesionej w pozycji
% siedzacej
q5 = [0 -pi/2 -pi/2 0 0 0];

% Czas wykonania ruchu nogi
t = 0:0.025:2;

% Trajektoria w przestrzeni zlaczy
if (wybor == 1)
    [q, qd, qdd] = jtraj(q1, q2, t);
elseif (wybor == 2)
    [q, qd, qdd] = jtraj(q1, q3, t);
elseif (wybor == 3)
    [q, qd, qdd] = jtraj(q4, q5, t);
end

% Zadanie proste kinematyki
T = Noga.fkine(q);
% Wektor pozycji koncowki roboczej (stopy) we wspolrz�dnych kartezjanskich
p = transl(T);

Wykresy;